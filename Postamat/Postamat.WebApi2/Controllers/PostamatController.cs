using Microsoft.AspNetCore.Mvc;
using Postamat.Entities;
using Postamat.Helpers;
using Postamat.LogicContracts;
using Postamat.WebApi.Entities;

namespace Postamat.WebApi.Controllers
{
    /// <summary>
    /// Контроллер для работы с Постаматом.
    /// </summary>
    [ApiController]
    [Route("api/v1/postamat")]
    public class PostamatController : Controller
    {
        private readonly IPostamatLogic _postamatLogic;

        public PostamatController(IPostamatLogic postamatLogic)
        {
            _postamatLogic = postamatLogic;
        }
        
        /// <summary>
        /// Возвращает информацию о Постамате.
        /// </summary>
        [HttpGet]
        [Route("{postamatNumber}")]
        public IActionResult Index(string postamatNumber)
        {
            var postamatResult = _postamatLogic.GetPostamatInfo(postamatNumber);
            
            if (postamatResult.CodeResponse != CodeResponse.Success)
            {
                return new JsonResult(postamatResult.CodeResponse.GetDescription());
            }

            var postamatModel = WebApiMapper.GetPostamatModel(postamatResult.Postamat);

            return new JsonResult(postamatModel);
        }
    }
}