using Microsoft.AspNetCore.Mvc;

using Postamat.Entities;
using Postamat.Helpers;
using Postamat.LogicContracts;
using Postamat.WebApi.Entities;
using Postamat.WebApi.Entities.Orders;

namespace Postamat.WebApi.Controllers
{
    /// <summary>
    /// Контроллер для работы с заказами.
    /// </summary>
    [ApiController]
    [Route("api/v1/order")]
    public class OrderController : Controller
    {
        private readonly IOrderLogic _orderLogic;

        public OrderController(IOrderLogic orderLogic)
        {
            _orderLogic = orderLogic;
        }
        
        /// <summary>
        /// Добавляет новый заказ.
        /// Возвращает результат операции.
        /// </summary>
        [HttpPost]
        [Route("add")]
        public IActionResult Add([FromBody]OrderModel orderModel)
        {
            if (orderModel == null)
            {
                return new JsonResult(CodeResponse.ErrorRequest.GetDescription());
            }

            var order = WebApiMapper.GetOrder(orderModel);

            var orderResult = _orderLogic.CreateOrder(order);
            if (orderResult.CodeResponse != CodeResponse.Success)
            {
                return new JsonResult(orderResult.CodeResponse.GetDescription());
            }

            var orderResultModel = WebApiMapper.GetOrderResultModel(orderResult);

            return new JsonResult(new
            {
                codeResponse = orderResultModel.CodeResponse.GetDescription(),
                orderNumber = orderResultModel.OrderNumber
            });
        }
        
        /// <summary>
        /// Изменяет существующий заказ.
        /// Возвращает результат операции.
        /// </summary>
        [HttpPost]
        [Route("change")]
        public IActionResult Change([FromBody]OrderModel orderModel)
        {
            if (orderModel == null)
            {
                return new JsonResult(CodeResponse.ErrorRequest.GetDescription());
            }

            var order = WebApiMapper.GetOrder(orderModel);

            var orderResult = _orderLogic.ChangeOrder(order);
            if (orderResult.CodeResponse != CodeResponse.Success)
            {
                return new JsonResult(orderResult.CodeResponse.GetDescription());
            }

            var orderResultModel = WebApiMapper.GetOrderResultModel(orderResult);

            return new JsonResult(new
            {
                codeResponse = orderResultModel.CodeResponse.GetDescription(),
                orderNumber = orderResultModel.OrderNumber
            });
        }
        
          
        /// <summary>
        /// Отменяет существующий заказ.
        /// Возвращает результат операции.
        /// </summary>
        [HttpPost]
        [Route("cancel")]
        public IActionResult Cancel(int orderNumber)
        {
            var orderResult = _orderLogic.CancelOrder(orderNumber);

            var orderResultModel = WebApiMapper.GetOrderResultModel(orderResult);
            if (orderResult.CodeResponse != CodeResponse.Success)
            {
                return new JsonResult(orderResult.CodeResponse.GetDescription());
            }

            return new JsonResult(new
            {
                codeResponse = orderResultModel.CodeResponse.GetDescription(),
                orderNumber = orderResultModel.OrderNumber
            });
        }
        
        /// <summary>
        /// Возвращает заказ по номеру.
        /// </summary>
        [HttpGet]
        [Route("{orderNumber}")]
        public IActionResult GetOrderInfo(int orderNumber)
        {
            var orderResult = _orderLogic.GetOrderInfo(orderNumber);

            var orderResultModel = WebApiMapper.GetOrderResultModel(orderResult);

            if (orderResult.CodeResponse != CodeResponse.Success)
            {
                return new JsonResult(orderResult.CodeResponse.GetDescription());
            }

            return new JsonResult(orderResultModel.Order);
        }
    }
}