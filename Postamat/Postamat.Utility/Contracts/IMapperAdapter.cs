using System.Net;

namespace Postamat.Utility.Contracts
{
    public interface IMapperAdapter
    {
        TDestination Map<TDestination>(object source) where TDestination : class;
    }
}