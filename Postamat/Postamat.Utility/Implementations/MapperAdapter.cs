using System.Net;
using AutoMapper;
using Postamat.Utility.Contracts;

namespace Postamat.Utility.Implementations
{
    public sealed class MapperAdapter : IMapperAdapter
    {
        public IMapper Mapper { get; }
        
        public MapperAdapter(IMapper mapper)
        {
            Mapper = mapper;
        }

        public TDestination Map<TDestination>(object source) where TDestination : class
        {
            return Mapper.Map<TDestination>(source);
        }
    }
}