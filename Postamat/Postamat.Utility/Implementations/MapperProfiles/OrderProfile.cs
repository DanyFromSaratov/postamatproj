using AutoMapper;
using Postamat.DAL.Entities;
using Postamat.Entities;

namespace Postamat.Utility.Implementations.MapperProfiles
{
    public class OrderProfile : Profile
    {
        public OrderProfile()
        {
            CreateOrderMaps();
        }
        
        private void CreateOrderMaps()
        {
            CreateMap<OrderModel, Order>();
            CreateMap<Order, OrderModel>();
            
            CreateMap<PostamatModel, PostamatInfo>();
            CreateMap<PostamatInfo, PostamatModel>();
            
            CreateMap<RecipientModel, Recipient>();
            CreateMap<Recipient, RecipientModel>();
        }
    }
}