using Postamat.Entities;

namespace Postamat.WebApi.Entities.Orders
{
    public class OrderResultModel
    {
        public int? OrderNumber { get; set; }
        
        public CodeResponse CodeResponse { get; set; }
        
        public OrderModel Order { get; set; }
    }
}