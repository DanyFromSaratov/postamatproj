using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Postamat.Entities.Orders;

namespace Postamat.WebApi.Entities.Orders
{
    /// <summary>
    /// Веб-модель заказа.
    /// </summary>
    public class OrderModel
    {  
        private IEnumerable<string> _products;
        
        /// <summary>
        /// Номер заказа.
        /// </summary>
        [JsonPropertyName("number")]
        public int Number { get; set; }

        /// <summary>
        /// Статус заказа.
        /// </summary>
        [JsonPropertyName("status")]
        public OrderStatus Status { get; set; }

        /// <summary>
        /// Название статуса.
        /// </summary>
        [JsonPropertyName("statusName")]
        public string StatusName { get; set; }

        /// <summary>
        /// Состав заказа.
        /// </summary>
        [JsonPropertyName("products")]
        public IEnumerable<string> Products 
        {
            get
            {
                if (_products == null || !_products.Any())
                {
                    return Enumerable.Empty<string>();
                }

                return _products;
            }
            set
            {
                _products = value;
            }
        }

        /// <summary>
        /// Стоимость заказа.
        /// </summary>
        [JsonPropertyName("price")]
        public decimal Price { get; set; }

        /// <summary>
        /// Номер постамата доставки.
        /// </summary>
        [JsonPropertyName("postamatNumber")]
        public string PostamatNumber { get; set; }

        /// <summary>
        /// Получатель.
        /// </summary>
        [JsonPropertyName("recipient")]
        public RecipientModel Recipient { get; set; }
        
    }
}