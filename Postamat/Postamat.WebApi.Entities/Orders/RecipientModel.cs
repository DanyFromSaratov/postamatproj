using System.Text.Json.Serialization;

namespace Postamat.WebApi.Entities.Orders
{
    /// <summary>
    /// Веб-модель получателя.
    /// </summary>
    public class RecipientModel
    { 
        /// <summary>
        /// Номер телефона.
        /// </summary>
        [JsonPropertyName("phoneNumber")]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Имя.
        /// </summary>
        [JsonPropertyName("firstName")]
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия.
        /// </summary>
        [JsonPropertyName("lastName")]
        public string LastName { get; set; }

        /// <summary>
        /// Отчество.
        /// </summary>
        [JsonPropertyName("middleName")]
        public string MiddleName { get; set; }
    }
}