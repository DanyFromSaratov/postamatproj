using Postamat.Entities.Orders;
using Postamat.Entities.Postamats;
using Postamat.Helpers;
using Postamat.WebApi.Entities.Orders;
using Postamat.WebApi.Entities.Postamats;

namespace Postamat.WebApi.Entities
{
    public static class WebApiMapper
    {
        public static Order GetOrder(OrderModel orderModel)
        {
            if (orderModel == null)
            {
                return null;
            }
            
            var recipient = GetRecipient(orderModel.Recipient);
            
            return new Order
            {
                Number = orderModel.Number,
                Products = orderModel.Products,
                PostamatNumber = orderModel.PostamatNumber,
                Price = orderModel.Price,
                Recipient = recipient,
                Status = orderModel.Status
            };
        }

        public static Recipient GetRecipient(RecipientModel recipientModel)
        {
            if (recipientModel == null)
            {
                return null;
            }

            return new Recipient
            {
                FirstName = recipientModel.FirstName,
                LastName = recipientModel.LastName,
                MiddleName = recipientModel.MiddleName,
                PhoneNumber = recipientModel.PhoneNumber
            };
        }
        
        public static OrderModel GetOrderModel(Order order)
        {
            if (order == null)
            {
                return null;
            }
            
            var recipient = GetRecipientModel(order.Recipient);
            
            return new OrderModel
            {
                Number = order.Number,
                Products = order.Products,
                PostamatNumber = order.PostamatNumber,
                Price = order.Price,
                Recipient = recipient,
                Status = order.Status,
                StatusName =  order.Status.GetDescription()
            };
        }

        public static RecipientModel GetRecipientModel(Recipient recipient)
        {
            if (recipient == null)
            {
                return null;
            }

            return new RecipientModel
            {
                FirstName = recipient.FirstName,
                LastName = recipient.LastName,
                MiddleName = recipient.MiddleName,
                PhoneNumber = recipient.PhoneNumber
            };
        }

        public static OrderResultModel GetOrderResultModel(OrderResult orderResult)
        {
            if (orderResult == null)
            {
                return null;
            }

            var orderModel = GetOrderModel(orderResult.Order);

            return new OrderResultModel
            {
                CodeResponse = orderResult.CodeResponse,
                Order = orderModel,
                OrderNumber = orderResult.OrderNumber
            };
        }
        
        public static PostamatModel GetPostamatModel(PostamatInfo postamatInfo)
        {
            if (postamatInfo == null)
            {
                return null;
            }

            return new PostamatModel
            {
                Number = postamatInfo.Number,
                Address = postamatInfo.Address,
                IsWork = postamatInfo.IsWork
            };
        }
    }
}