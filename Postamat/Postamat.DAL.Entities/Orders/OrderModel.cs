﻿using System.Collections.Generic;
using System.Linq;

namespace Postamat.DAL.Entities.Orders
{
    /// <summary>
    /// Модель заказа
    /// </summary>
    public class OrderModel
    {
        private IEnumerable<string> _products;
        
        /// <summary>
        /// Номер заказа.
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Статус заказа.
        /// </summary>
        public OrderStatusModel Status { get; set; }

        /// <summary>
        /// Состав заказа.
        /// </summary>
        public IEnumerable<string> Products 
        {
            get
            {
                if (_products == null || !_products.Any())
                {
                    return Enumerable.Empty<string>();
                }

                return _products;
            }
            set
            {
                _products = value;
            }
        }

        /// <summary>
        /// Стоимость заказа.
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Номер постамата доставки.
        /// </summary>
        public string PostamatNumber { get; set; }

        /// <summary>
        /// Получатель.
        /// </summary>
        public RecipientModel Recipient { get; set; }
    }
}