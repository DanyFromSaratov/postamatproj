namespace Postamat.DAL.Entities.Orders
{
    /// <summary>
    /// Модель статуса заказа.
    /// </summary>
    public enum OrderStatusModel : int
    {
        /// <summary>
        /// Зарегистрирован.
        /// </summary>
        Registered = 1,
        
        /// <summary>
        /// Принят на складе.
        /// </summary>
        AcceptedInWarehouse = 2,
        
        /// <summary>
        /// Выдан курьеру.
        /// </summary>
        IssuedToCourier = 3,
        
        /// <summary>
        /// Доставлен в постамат.
        /// </summary>
        DeliveredToPostamat = 4,
        
        /// <summary>
        /// Доставлен получателю.
        /// </summary>
        DeliveredToRecipient = 5,
        
        /// <summary>
        /// Отменен.
        /// </summary>
        Canceled = 6,
    }
}