namespace Postamat.DAL.Entities.Postamats
{
    /// <summary>
    /// Модель Постамата.
    /// </summary>
    public class PostamatModel
    { 
        /// <summary>
        /// Номер.
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Адрес.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Признак работы автомата: Рабочий, иначе закрыт.
        /// </summary>
        public bool IsWork { get; set; }
    }
}