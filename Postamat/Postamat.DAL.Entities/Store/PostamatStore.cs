using System.Collections.Generic;
using System.Linq;
using Postamat.DAL.Entities.Postamats;

namespace Postamat.DAL.Entities.Store
{
    public class PostamatStore
    {
        private static List<PostamatModel> _postamats;

        private PostamatStore()
        {
            _postamats = new List<PostamatModel>
            {
                new PostamatModel
                {
                    Number = "РТ-1234",
                    Address = "ул. Тест дом 1234",
                    IsWork = true
                },
                new PostamatModel
                {
                    Number = "РТ-2345",
                    Address = "ул. Тест2 дом 2345",
                    IsWork = false
                }
            };
        }
        
        private static PostamatStore _instance;
        
        public static PostamatStore GetInstance()
        {
            if (_instance == null)
            {
                _instance = new PostamatStore();
            }
            
            return _instance;
        }

        public static PostamatModel GetPostamatInfo(string postamatNumber)
        {
            return _postamats.FirstOrDefault(postamat => postamat.Number == postamatNumber);
        }
    }
}