﻿using Postamat.Entities;
using Postamat.Entities.Orders;

namespace Postamat.LogicContracts
{
    /// <summary>
    /// Сервис для работы с заказами.
    /// </summary>
    public interface IOrderLogic
    {
        /// <summary>
        /// Создает заказ.
        /// </summary>
        OrderResult CreateOrder(Order order);

        /// <summary>
        /// Изменяет заказ.
        /// </summary>
        OrderResult ChangeOrder(Order order);

        /// <summary>
        /// Отменяет заказ.
        /// </summary>
        OrderResult CancelOrder(int orderNumber);

        /// <summary>
        /// Возвращает информацию по заказу.
        /// </summary>
        OrderResult GetOrderInfo(int orderNumber);
    }
}