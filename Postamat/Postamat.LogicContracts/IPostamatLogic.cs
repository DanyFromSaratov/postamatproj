using Postamat.Entities.Postamats;

namespace Postamat.LogicContracts
{
    /// <summary>
    /// Сервис для работы с Постаматом.
    /// </summary>
    public interface IPostamatLogic
    {
        /// <summary>
        /// Возвращает инофрмацию о Постомате.
        /// </summary>
        PostamatResult GetPostamatInfo(string postamatNumber);
    }
}