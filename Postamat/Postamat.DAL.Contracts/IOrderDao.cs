﻿using Postamat.DAL.Entities;
using Postamat.DAL.Entities.Orders;

namespace Postamat.DAL.Contracts
{
    /// <summary>
    /// DAL для работы с заказами.
    /// </summary>
    public interface IOrderDao
    {
        /// <summary>
        /// Создает заказ.
        /// Возвращает номер заказа.
        /// </summary>
        int? CreateOrder(OrderModel order);

        /// <summary>
        /// Изменяет заказ.
        /// </summary>
        int? ChangeOrder(OrderModel orderModel);

        /// <summary>
        /// Отменяет заказ.
        /// </summary>
        int? CancelOrder(int orderNumber);

        /// <summary>
        /// Возвращает информацию по заказу.
        /// </summary>
        OrderModel GetOrderInfo(int orderNumber);

        /// <summary>
        /// Возвращает признак того,
        /// существует ли заказ.
        /// </summary>
        bool IsOrderExists(int orderNumber);
    }
}