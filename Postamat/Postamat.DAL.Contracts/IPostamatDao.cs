using Postamat.DAL.Entities;
using Postamat.DAL.Entities.Postamats;

namespace Postamat.DAL.Contracts
{
    /// <summary>
    /// DAL для работы с Постаматом.
    /// </summary>
    public interface IPostamatDao
    {
        /// <summary>
        /// Возвращает инофрмацию о Постомате.
        /// </summary>
        PostamatModel GetPostamatInfo(string postamatNumber);
    }
}