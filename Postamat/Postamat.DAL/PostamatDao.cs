using Postamat.DAL.Contracts;
using Postamat.DAL.Entities;
using Postamat.DAL.Entities.Postamats;
using Postamat.DAL.Entities.Store;

namespace Postamat.DAL
{
    /// <summary>
    /// DAL для работы с Постаматом.
    /// </summary>
    public class PostamatDao : IPostamatDao
    {
        /// <summary>
        /// Возвращает инофрмацию о Постомате.
        /// </summary>
        public PostamatModel GetPostamatInfo(string postamatNumber)
        {
            return PostamatStore.GetPostamatInfo(postamatNumber);
        }
    }
}