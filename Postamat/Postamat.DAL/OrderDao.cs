﻿using System;
using System.Collections.Generic;
using System.Linq;

using Postamat.DAL.Contracts;
using Postamat.DAL.Entities;
using Postamat.DAL.Entities.Orders;

namespace Postamat.DAL
{
    /// <summary>
    /// DAL для работы с заказами.
    /// </summary>
    public class OrderDao : IOrderDao
    {
        private static Dictionary<int, OrderModel> _orders;
        
        static OrderDao()
        {
            _orders = new Dictionary<int, OrderModel>();
        }
        
        /// <summary>
        /// Создает заказ.
        /// Возвращает номер заказа.
        /// </summary>
        public int? CreateOrder(OrderModel order)
        {
            if (order == null)
            {
                return null;
            }

            try
            {
                order.Number = GetNewOrderNumber();
                order.Status = OrderStatusModel.Registered;

                _orders.Add(order.Number, order);

                return order.Number;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        /// <summary>
        /// Изменяет заказ.
        /// </summary>
        public int? ChangeOrder(OrderModel orderModel)
        {
            if (orderModel == null)
            {
                return null;
            }

            try
            {
                if (!_orders.ContainsKey(orderModel.Number))
                {
                    return null;
                }

                _orders[orderModel.Number] = orderModel;

                return orderModel.Number;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        /// <summary>
        /// Отменяет заказ.
        /// </summary>
        public int? CancelOrder(int orderNumber)
        {
            try
            {
                var orderModel =  _orders[orderNumber];
                orderModel.Status = OrderStatusModel.Canceled;

                _orders[orderNumber] = orderModel; 

                return orderNumber;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        /// <summary>
        /// Возвращает информацию по заказу.
        /// </summary>
        public OrderModel GetOrderInfo(int orderNumber)
        {
            return !_orders.ContainsKey(orderNumber)
                ? null 
                : _orders[orderNumber];
        }

        /// <summary>
        /// Возвращает признак того,
        /// существует ли заказ.
        /// </summary>
        public bool IsOrderExists(int orderNumber)
        {
            return _orders.ContainsKey(orderNumber);
        }

        /// <summary>
        /// Возвращает номер созданного заказа.
        /// </summary>
        /// <returns></returns>
        private int GetNewOrderNumber()
        {
            return _orders.Keys.Any() 
                ?_orders.Keys.Max() + 1
                : 1;
        }
    }
}