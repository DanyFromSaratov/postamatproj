using Postamat.DAL.Entities.Orders;
using Postamat.DAL.Entities.Postamats;
using Postamat.Entities.Orders;
using Postamat.Entities.Postamats;

namespace Postamat.Entities
{
    public static class Mapper
    {
        public static Order GetOrder(OrderModel orderModel)
        {
            if (orderModel == null)
            {
                return null;
            }

            var recipient = GetRecipient(orderModel.Recipient);

            return new Order
            {
                Products = orderModel.Products,
                PostamatNumber = orderModel.PostamatNumber,
                Price = orderModel.Price,
                Recipient = recipient,
                Number = orderModel.Number,
                Status = (OrderStatus)orderModel.Status
            };
        }

        public static Recipient GetRecipient(RecipientModel recipientModel)
        {
            if (recipientModel == null)
            {
                return null;
            }

            return new Recipient
            {
                FirstName = recipientModel.FirstName,
                LastName = recipientModel.LastName,
                MiddleName = recipientModel.MiddleName,
                PhoneNumber = recipientModel.PhoneNumber
            };
        }

        public static OrderModel GetOrderModel(Order order)
        {
            if (order == null)
            {
                return null;
            }

            var recipient = GetRecipientModel(order.Recipient);

            return new OrderModel
            {
                Number = order.Number,
                Products = order.Products,
                PostamatNumber = order.PostamatNumber,
                Price = order.Price,
                Recipient = recipient
            };
        }

        public static RecipientModel GetRecipientModel(Recipient recipient)
        {
            if (recipient == null)
            {
                return null;
            }

            return new RecipientModel
            {
                FirstName = recipient.FirstName,
                LastName = recipient.LastName,
                MiddleName = recipient.MiddleName,
                PhoneNumber = recipient.PhoneNumber
            };
        }

        public static PostamatInfo GetPostamatInfo(PostamatModel postamatModel)
        {
            if (postamatModel == null)
            {
                return null;
            }

            return new PostamatInfo
            {
                Number = postamatModel.Number,
                Address = postamatModel.Address,
                IsWork = postamatModel.IsWork
            };
        }

        public static object GetOrderResultModel(OrderResult orderResult)
        {
            throw new System.NotImplementedException();
        }
    }
}