namespace Postamat.Entities.Orders
{
    public class OrderResult
    {
        public int? OrderNumber { get; set; }

        public CodeResponse CodeResponse { get; set; }

        public Order Order { get; set; }
    }
}