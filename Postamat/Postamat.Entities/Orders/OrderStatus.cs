using System.ComponentModel;

namespace Postamat.Entities.Orders
{
    /// <summary>
    /// Статус заказа.
    /// </summary>
    public enum OrderStatus : int
    {
        /// <summary>
        /// Зарегистрирован.
        /// </summary>
        [Description("Зарегистрирован")]
        Registered = 1,
        
        /// <summary>
        /// Принят на складе.
        /// </summary>
        [Description("Принят на складе")]
        AcceptedInWarehouse = 2,
        
        /// <summary>
        /// Выдан курьеру.
        /// </summary>
        [Description("Выдан курьеру")]
        IssuedToCourier = 3,
        
        /// <summary>
        /// Доставлен в постамат.
        /// </summary>
        [Description("Доставлен в постамат")]
        DeliveredToPostamat = 4,
        
        /// <summary>
        /// Доставлен получателю.
        /// </summary>
        [Description("Доставлен получателю")]
        DeliveredToRecipient = 5,
        
        /// <summary>
        /// Отменен.
        /// </summary>
        [Description("Отменен")]
        Canceled = 6,
    }
}