namespace Postamat.Entities.Postamats
{
    /// <summary>
    /// Постамат.
    /// </summary>
    public class PostamatInfo
    {
        /// <summary>
        /// Номер.
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Адрес.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Признак работы автомата: Рабочий, иначе закрыт.
        /// </summary>
        public bool IsWork { get; set; }
    }
}