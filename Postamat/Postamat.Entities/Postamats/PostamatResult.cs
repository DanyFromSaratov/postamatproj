namespace Postamat.Entities.Postamats
{
    public class PostamatResult
    {
        public int? OrderNumber { get; set; }

        public CodeResponse CodeResponse { get; set; }

        public PostamatInfo Postamat { get; set; }
    }
}