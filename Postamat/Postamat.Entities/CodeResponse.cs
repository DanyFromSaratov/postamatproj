using System.ComponentModel;

namespace Postamat.Entities
{
    /// <summary>
    /// Код ответа.
    /// </summary>
    public enum CodeResponse : short
    {
        /// <summary>
        /// Ok.
        /// </summary>
        [Description("OK")]
        Success = 0,
        
        /// <summary>
        /// Заказ не найден.
        /// </summary>
        [Description("Заказ не найден")]
        OrderNotFound = 1,
        
        /// <summary>
        /// Постамат не найден.
        /// </summary>
        [Description("Постамат не найден")]
        PostamatNotFound = 2,
        
        /// <summary>
        /// Запрещено.
        /// </summary>
        [Description("Запрещено")]
        RegisterForbidden = 3,
        
        /// <summary>
        /// Ошибка запроса.
        /// </summary>
        [Description("Ошибка запроса")]
        ErrorRequest = 4,
    }
}