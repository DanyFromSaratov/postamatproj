﻿using Postamat.DAL.Contracts;
using Postamat.DAL.Entities.Orders;
using Postamat.Entities;
using Postamat.Entities.Orders;
using Postamat.LogicContracts;

namespace Postamat.Logic
{
    /// <summary>
    /// Сервис для работы с заказами.
    /// </summary>
    public class OrderLogic : IOrderLogic
    {
        private readonly IOrderDao _orderDao;
        private readonly IPostamatLogic _postamatLogic;

        public OrderLogic(
            IOrderDao orderDao,
            IPostamatLogic postamatLogic)
        {
            _orderDao = orderDao;
            _postamatLogic = postamatLogic;
        }
        
        /// <summary>
        /// Создает заказ.
        /// Возвращает номер заказа.
        /// </summary>
        public OrderResult CreateOrder(Order order)
        {
            var codeResponse = Validator.ValidateOrder(order);
            if (codeResponse != CodeResponse.Success)
            {
                return new OrderResult
                {
                    CodeResponse = codeResponse
                };
            }

            var postamatResult = _postamatLogic.GetPostamatInfo(order.PostamatNumber);
            if (postamatResult.CodeResponse != CodeResponse.Success)
            {
                return new OrderResult
                {
                    CodeResponse = postamatResult.CodeResponse
                };
            }

            if (!postamatResult.Postamat.IsWork)
            {
                return new OrderResult
                {
                    CodeResponse = CodeResponse.RegisterForbidden
                };
            }

            var orderModel = Mapper.GetOrderModel(order);

            var orderNumber = _orderDao.CreateOrder(orderModel);
            if (orderNumber.HasValue)
            {
                return new OrderResult
                {
                    OrderNumber = orderNumber,
                    CodeResponse = CodeResponse.Success
                };
            }

            return new OrderResult
            {
                CodeResponse = CodeResponse.ErrorRequest
            };
        }
        
        /// <summary>
        /// Изменяет заказ.
        /// </summary>
        public OrderResult ChangeOrder(Order order)
        {
            var codeResponse = Validator.ValidateOrder(order);
            if (codeResponse != CodeResponse.Success)
            {
                return new OrderResult
                {
                    CodeResponse = codeResponse
                };
            }

            var postamatResult = _postamatLogic.GetPostamatInfo(order.PostamatNumber);
            if (postamatResult.CodeResponse != CodeResponse.Success)
            {
                return new OrderResult
                {
                    CodeResponse = postamatResult.CodeResponse
                };
            }
            
            var isOrderExists = _orderDao.IsOrderExists(order.Number);
            if (!isOrderExists)
            {
                return new OrderResult
                {
                    CodeResponse = CodeResponse.OrderNotFound
                };
            }

            var currentOderModel = _orderDao.GetOrderInfo(order.Number);

            FillOrderModel(currentOderModel, order);
            
            var orderNumber = _orderDao.ChangeOrder(currentOderModel);
            if (!orderNumber.HasValue)
            {
                return new OrderResult
                {
                    CodeResponse = CodeResponse.ErrorRequest
                };
            }

            return new OrderResult
            {
                CodeResponse = CodeResponse.Success,
                OrderNumber = orderNumber
            };
        }

        /// <summary>
        /// Отменяет заказ.
        /// </summary>
        public OrderResult CancelOrder(int orderNumber)
        {
            var isOrderExists = _orderDao.IsOrderExists(orderNumber);
            if (!isOrderExists)
            {
                return new OrderResult
                {
                    CodeResponse = CodeResponse.OrderNotFound
                };
            }

            var canceledOrderNumber = _orderDao.CancelOrder(orderNumber);
            if (!canceledOrderNumber.HasValue)
            {
                return new OrderResult
                {
                    CodeResponse = CodeResponse.ErrorRequest
                }; 
            }

            return new OrderResult
            {
                OrderNumber = canceledOrderNumber,
                CodeResponse = CodeResponse.Success
            };
        }

        /// <summary>
        /// Возвращает информацию по заказу.
        /// </summary>
        public OrderResult GetOrderInfo(int orderNumber)
        {
            var isOrderExists = _orderDao.IsOrderExists(orderNumber);
            if (!isOrderExists)
            {
                return new OrderResult
                {
                    CodeResponse = CodeResponse.OrderNotFound
                };
            }

            var orderModel = _orderDao.GetOrderInfo(orderNumber);
            if (orderModel == null)
            {
                return new OrderResult
                {
                    CodeResponse = CodeResponse.ErrorRequest
                }; 
            }

            var order = Mapper.GetOrder(orderModel);

            return new OrderResult
            {
                CodeResponse = CodeResponse.Success,
                Order = order
            };
        }

        /// <summary>
        /// Заполняет данные заказа.
        /// </summary>
        private static void FillOrderModel(OrderModel currentOderModel, Order order)
        {
            var recipientModel = Mapper.GetRecipientModel(order.Recipient);
            
            currentOderModel.Price = order.Price;
            currentOderModel.Status = (OrderStatusModel) order.Status;
            currentOderModel.Recipient = recipientModel;
            currentOderModel.Products = order.Products;
        }
    }
}