using System.Linq;
using System.Text.RegularExpressions;
using Postamat.Entities;
using Postamat.Entities.Orders;

namespace Postamat.Logic
{
    public static class Validator
    {
        private const string PostamatNumberRegex = @"РТ-\d{4}";
        private const string PhoneNumberRegex = @"\+7\d{3}-\d{3}-\d{2}-\d{2}";
        
        /// <summary>
        /// Проверяет на корректность заказ.
        /// Возвращает результат проверки.
        /// </summary>
        public static CodeResponse ValidateOrder(Order order)
        {
            if (order == null)
            {
                return CodeResponse.ErrorRequest;
            }

            var products = order.Products.ToList();

            if (products.Count > 10)
            {
                return CodeResponse.ErrorRequest;
            }

            if (string.IsNullOrWhiteSpace(order.PostamatNumber))
            {
                return CodeResponse.PostamatNotFound;
            }

            var isPhoneNumberValid = Regex.IsMatch(order.Recipient.PhoneNumber, PhoneNumberRegex);
            if (!isPhoneNumberValid)
            {
                return CodeResponse.ErrorRequest;
            }

            return CodeResponse.Success;
        }
        
        
        public static CodeResponse ValidatePostamatNumber(string postamatNumber)
        {
            if (string.IsNullOrWhiteSpace(postamatNumber))
            {
                return CodeResponse.ErrorRequest;
            }

            var postamatNumberArray = postamatNumber.Split("-");


            var isValid = Regex.IsMatch(postamatNumber, PostamatNumberRegex);
            if (!isValid)
            {
                return CodeResponse.ErrorRequest;
            }

            return CodeResponse.Success;
        }
    }
}