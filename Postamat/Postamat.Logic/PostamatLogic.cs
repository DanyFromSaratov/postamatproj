using Postamat.DAL.Contracts;
using Postamat.Entities;
using Postamat.Entities.Postamats;
using Postamat.LogicContracts;

namespace Postamat.Logic
{
    /// <summary>
    /// Сервис для работы с Постаматом.
    /// </summary>
    public class PostamatLogic : IPostamatLogic
    {
        private readonly IPostamatDao _postamatDao;

        public PostamatLogic(IPostamatDao postamatDao)
        {
            _postamatDao = postamatDao;
        }
        
        /// <summary>
        /// Возвращает инофрмацию о Постомате.
        /// </summary>
        public PostamatResult GetPostamatInfo(string postamatNumber)
        {
            var validateResult = Validator.ValidatePostamatNumber(postamatNumber);
            if (validateResult != CodeResponse.Success)
            {
                return new PostamatResult
                {
                    CodeResponse = validateResult
                };
            }

            var postamatModel = _postamatDao.GetPostamatInfo(postamatNumber);
            if (postamatModel == null)
            {
                return new PostamatResult
                {
                    CodeResponse = CodeResponse.PostamatNotFound
                };
            }

            var postamatInfo = Mapper.GetPostamatInfo(postamatModel);

            return new PostamatResult
            {
                Postamat = postamatInfo,
                CodeResponse = CodeResponse.Success
            };
        }
    }
}