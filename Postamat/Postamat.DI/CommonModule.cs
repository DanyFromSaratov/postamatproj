﻿using Autofac;

using Postamat.DAL;
using Postamat.DAL.Contracts;
using Postamat.DAL.Entities.Store;
using Postamat.Logic;
using Postamat.LogicContracts;

namespace Postamat.DI
{
    public class CommonModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            RegisterCommonModules(builder);
            RegisterBllDependencies(builder);
            RegisterDalDependencies(builder);
        }

        private void RegisterCommonModules(ContainerBuilder builder)
        {
            PostamatStore.GetInstance();
        }

        private void RegisterBllDependencies(ContainerBuilder builder)
        {
            builder.RegisterType<OrderLogic>()
                .As<IOrderLogic>()
                .SingleInstance();
            
            builder.RegisterType<PostamatLogic>()
                .As<IPostamatLogic>()
                .SingleInstance();
        }
        
        private void RegisterDalDependencies(ContainerBuilder builder)
        {
            builder.RegisterType<OrderDao>()
                .As<IOrderDao>()
                .SingleInstance();
            
            builder.RegisterType<PostamatDao>()
                .As<IPostamatDao>()
                .SingleInstance();
        }
    }
}