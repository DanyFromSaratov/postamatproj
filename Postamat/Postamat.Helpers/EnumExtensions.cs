﻿using System;
using System.ComponentModel;
using System.Linq;

namespace Postamat.Helpers
{
    /// <summary>
    /// Методы-расширения для перечислений.
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// Значение атрибута Description у перечисления.
        /// </summary>
        /// <param name="enumObject">Перечисление.</param>
        /// <returns>Текстовый дескриптор.</returns>
        public static string GetDescription(this Enum enumObject)
        {
            if (enumObject == null)
            {
                return string.Empty;
            }

            var fieldName = enumObject.ToString();
            var fieldInfo = enumObject
                .GetType()
                .GetField(fieldName);

            return fieldInfo == null
                ? fieldName
                : fieldInfo
                    .GetCustomAttributes(typeof(DescriptionAttribute), false)
                    .Cast<DescriptionAttribute>()
                    .Select(x => x.Description)
                    .DefaultIfEmpty(fieldName)
                    .First();
        }
    }
}